# Simple Auth Proxy

The aim of this application was originally to protect an AWS Elasticsearch / Kibana service. Although ultimately, this proxy doesnt contain anything specific to Elasticsearch / Kibana and it could be used to proxy other HTTP based serviced

To start the proxy you can run something like the following

`npm start`

This will use the configuration in `config/auth-proxy.yml`. By default this will start a secure server using the test certificates and key located in `test-pki` and will also enable basic auth, using a simple test user defined in `config/users.htpasswd` Although this is fine for testing, you should create your own certificates and users file

**Do not use these SSL keys / certificates in a production environment**

## Configuration Options

All of the following options sit under the `server` yaml construct in the configuration file. You can also provide many of these options as Enviroment Variables

* `port` - Defaults to `3000`. Defines the port the proxy will bind to. Can also use the `PORT` Environment Variable

* `host` - Defaults to `localhost`. Defines the host the proxy will bind to (e.g 0.0.0.0). Can also use the `HOST ` Environment Variable

* `target` - Defines the target url for which to proxy to. Can also use the `PROXY_TARGET ` Environment Variable

* `secure_enabled` - Defaults to `false`. This determines if to use SSL / TLS or use an insecure server. Can also use the `SSL_ENABLED` Environment Variable

* `secure_key` - The private key to use to use in pem format if using SSL / TLS. Can also use the `SSL_KEY ` Environment Variable

* `secure_certificate` - The public certificate to use in pem format if using SSL / TLS. Can also use the `SSL_CERTIFICATE` Environment Variable

* `secure_ca` - The CA bundle, usually combining the intermediate and root certificates in pem format if using SSL / TLS. Can also use the `SSL_CA` Environment Variable

* `basic_auth` - Defaults to `false`. If this is set to true, a `users.htpasswd` file is expected to be in the root directory. Can also use the `BASIC_AUTH` Environment Variable

* `root_rewrite` - If provided, will redirect the user to the value provided if they access the root. Can also use the `ROOT_REWRITE` Environment Variable

## Configuration Information

### Basic Auth
If using `basic_auth`, it is highly recommended that you also enable and use SSL / TLS to ensure that credential information is transmitted securely

Some test keys / certificates are included in the distribution to allow testing of the SSL / TLS configuration.
**Do not use these SSL keys / certificates in a production environment**

## Docker

A Dockerfile has been included to allow this application to be run as a containerised application.

You can build the image by running something like:

```docker image build -t authentication-proxy:latest .```

Before running the container, you should consider your proxy target. If the proxy target is hosted as a containerised app then using Docker networking to access the target may be an option.
If your target is on the host system (e.g. available via localhost on the host) then the way you access the target from the container can vary depending on your OS.

For example, using Mac OSX you can set your target to be `host.docker.internal`. This will resolve to the interval IP address that is used by the host. **Note:** This will not work on Linux

In Linux, You should be able to grab the IP address of the docker0 bridge for the host. For example:

```
sudo ip addr show docker0

3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default
    link/ether 02:42:9e:3d:40:4d brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
```

This would indicate that the host would be available at the IP address `172.17.0.1`

Another alternative on Linux is to start your container and provide `--network="host"` but this generally isn't recommended as removes all network isolation away from the container. Esentially this means if you expose a port, this will be available via the host without explict port mappings

### Docker Run Examples

The following starts up a container with Basic Authentication disabled and the Proxy target set to be a service on the host

**Note:** As mentioned above, `host.docker.internal` will not work on Linux

```
docker container run -d \
-e PROXY_TARGET='http://host.docker.internal:9200' \
-e BASIC_AUTH='false' \
-p 3000:3000 auth-proxy:latest
```

The following mounts a host local `users.htpasswd` file that overrides the file baked into the image

```
docker container run -d \
-e PROXY_TARGET='http://host.docker.internal:9200' \
-e BASIC_AUTH='true' \
-v "$(pwd)"/override_users.htpasswd:/home/node/app/config/users.htpasswd \
-p 3000:3000 auth-proxy:latest
```

## Pushing to AWS ECR

The ECR Repository gives you instructions on how to login, build and push your docker image. The steps are broadly outlined below.

```
# Docker Login
$(aws ecr get-login --profile <profile> --no-include-email --region <region>)

# Build Image
docker build -t authentication-proxy:latest .

# You need to tag the build in a certain way
docker tag static-file-server:latest <account>.dkr.ecr.<region>.amazonaws.com/authentication-proxy:latest
docker tag static-file-server:latest <account>.dkr.ecr.<region>.amazonaws.com/authentication-proxy:<version>

# Push the image to ECR
docker push <account>.dkr.ecr.<region>.amazonaws.com/authentication-proxy:latest
docker push <account>.dkr.ecr.<region>.amazonaws.com/authentication-proxy:<version>
```
