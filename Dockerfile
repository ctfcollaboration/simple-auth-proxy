FROM node:8-alpine

USER node

RUN mkdir -p /home/node/app
WORKDIR /home/node/app

COPY package.json .
RUN npm install --production

COPY . ./

EXPOSE 3000

ENTRYPOINT ["npm", "start"]
