var figlet = require('figlet'),
    log4js = require('log4js'),
    utils = require('./src/utils.js'),
    Configuration = require('./src/classes/configuration.js'),
    HttpServerBuilder = require('./src/classes/httpServerBuilder.js'),
    ProxyServer = require('./src/classes/proxyServer.js'),
    bodyParser = require('body-parser')

// Load the Configuration
let { server: serverConfig } = new Configuration().getProperties()

log4js.configure({
  appenders: {
    'out': { type: 'stdout', layout: { type: 'basic' } },
    'audit' : { type: 'file', filename: serverConfig['audit_log'] }
  },
  categories: {
    default: { appenders: ['out'], level: 'info' },
    audit: { appenders: ['audit'], level: 'info' }
  }
});

const logger = log4js.getLogger();

console.log(figlet.textSync('SB Auth Proxy', {
  horizontalLayout: 'default',
  verticalLayout: 'default'
}));

// Server Port
let server_port = process.env.PORT || serverConfig["port"] || 3000

// Server Host
let server_host = process.env.HOST || serverConfig["host"] || 'localhost'

// ProxyServer
let proxy = new ProxyServer(serverConfig)

// HttpServer
let httpServerBuilder = new HttpServerBuilder(serverConfig)

// JsonParser
let jsonParser = bodyParser.json()

let httpServer = httpServerBuilder.build((req, res) => {

    // TODO: Conditionally Enable / Disable this
    //jsonParser(req, res, (error) => {
    //  if (req.body) {
    //    logger.info(JSON.stringify(req.body))
    //  }
    //})

    proxy.process(req, res)
})

// Start Server
httpServer.listen(server_port, server_host, () => {
  logger.info(`Auth Proxy Started on port: ${server_port}, host: ${server_host}`)

  if (httpServerBuilder.isBasicAuth()) {
    logger.info(`Audit Logs are located at ${serverConfig['audit_log']}`)
  }
})

// SIGTERM Listener
process.on("SIGTERM", () => {
  logger.info("Shutting Down Proxy")
  proxy.close(function() {
    logger.info("Proxy Closed")
  })
  httpServer.close()
})
