const logger = require('log4js').getLogger()

exports.getValueOrDefault = (value, defaultValue) => {
  return typeof value === 'undefined' ? defaultValue : value
}

exports.getValueOrTerminate = (value, errorMessage) => {
  if (typeof value === 'undefined') {
    console.log(errorMessage)
    process.exit(1)
  } else {
    return value
  }
}

exports.exitError = (message) => {
  logger.error(message)
  logger.error("Stopping Auth Proxy")
  process.exit()
}
