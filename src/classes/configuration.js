const logger = require('log4js').getLogger(),
      safeLoad = require('js-yaml').safeLoad,
      read = require('fs').readFileSync;

class Configuration {

  constructor() {
    try {
      this.config = safeLoad(read('./config/auth-proxy.yml'), 'utf-8')

      if (typeof this.config.server === 'undefined') {
        logger.error("Configuration file does not seem to contain expected values")
        process.exit(1)
      }

      logger.info("Configuration successfully loaded")

    } catch(error) {
      logger.error("Unable to read configuration file: ", error.message)
      process.exit(1)
    }
  }

  getProperties() {
    return this.config
  }

}

module.exports = Configuration
