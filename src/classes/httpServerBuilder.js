const http = require('http'),
      https = require('https'),
      auth = require('http-auth'),
      path = require('path'),
      log4js = require('log4js'),
      read = require('fs').readFileSync,
      utils = require('../utils.js');

const logger = log4js.getLogger()
const auditLog = log4js.getLogger('audit')

const BASE_DIR = `${__dirname}/../../`
const HT_USER_FILE = `config/users.htpasswd`
const SECURE_DEFAULT = false
const BASIC_AUTH_ENABLED_DEFAULT = false

class HttpServerBuilder {

  constructor(serverConfig) {

    this.basic_auth = JSON.parse(process.env.BASIC_AUTH || utils.getValueOrDefault(serverConfig["basic_auth"], BASIC_AUTH_ENABLED_DEFAULT))
    this.secure = JSON.parse(process.env.SSL_ENABLED || utils.getValueOrDefault(serverConfig["secure_enabled"], SECURE_DEFAULT))

    if (this.secure) {

      this.secure_key = process.env.SSL_KEY || serverConfig["secure_key"]
      this.secure_certificate = process.env.SSL_CERTIFICATE || serverConfig["secure_certificate"]
      this.secure_ca = process.env.SSL_CA || serverConfig["secure_ca"]

      if (!this.secure_key || !this.secure_certificate || !this.secure_ca) {
        utils.exitError("'secure_key', 'secure_certificate' and 'secure_ca' need to be provided in the yaml config if 'secure_enabled'")
      }
    }
  }

  isBasicAuth() {
    return this.basic_auth
  }

  isSecure() {
    return this.secure
  }

  build(listener) {
    let serverArguments = this._buildServerArguments(listener)

    if (this.secure) {
      logger.info("Building SSL / TLS Server")
      return https.createServer.apply(https, serverArguments)

    } else {
      logger.warn("Building non secure HTTP Server. This is a security risk. Consider using SSL / TLS")
      return http.createServer.apply(http, serverArguments)
    }
  }

  _buildServerArguments(listener) {
    let serverArguments = new Array();

    if (this.basic_auth) {
      let basic = auth.basic({ realm: "78b9242a98ef", file: BASE_DIR + HT_USER_FILE })
      HttpServerBuilder._registerBasicAuthListeners(basic)

      serverArguments.push(basic)
    } else {
      logger.warn("Basic Auth is disabled. This is a security risk. Consider using basic auth over SSL / TLS")
    }

    if (this.secure) {
      try {
        let ssl_options = {
          key: read(path.isAbsolute(this.secure_key) ? this.secure_key : BASE_DIR + this.secure_key),
          cert: read(path.isAbsolute(this.secure_certificate) ? this.secure_certificate : BASE_DIR + this.secure_certificate),
          ca: read(path.isAbsolute(this.secure_ca) ? this.secure_ca : BASE_DIR + this.secure_ca),
          ciphers: 'ECDHE-RSA-AES128-SHA256:AES128-GCM-SHA256:!DES:!RC4:HIGH:!MD5:!aNULL:!EDH',
          honorCipherOrder: true
        }
        serverArguments.push(ssl_options)
      } catch(e) {
        utils.exitError(`There was an error loading the PKI files:\n\t${e.message}`)
      }
    }

    if (listener) {
      serverArguments.push(listener)
    }

    return serverArguments
  }

  static _registerBasicAuthListeners(basic) {

    let auditFunction = function(result, req) {
      auditLog.info(`Authentication ${result.pass ? 'Success' : 'Failure'} - user: ${result.user}, remoteAddress: ${req.connection.remoteAddress}, Request: ${req.url}`);
    }

    basic.on('success', auditFunction)
    basic.on('fail', auditFunction)
  }

}

module.exports = HttpServerBuilder
