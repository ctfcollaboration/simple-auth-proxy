const httpProxy = require('http-proxy'),
      url = require('url'),
      logger = require('log4js').getLogger(),
      utils = require('../utils.js')

class ProxyServer {

  constructor(serverConfig) {
    this.target_url = process.env.PROXY_TARGET || serverConfig["target"]
    this.root_rewrite = process.env.ROOT_REWRITE || serverConfig["root_rewrite"]

    if (!this.target_url) {
      logger.error("Proxy Target is MISSING")
      logger.error("Set 'server.target' the the yaml config or PROXY_TARGET environment variable")
      process.exit(1)
    }
    logger.info(`Proxy Target: ${this.target_url}`)

    this.proxy = httpProxy.createProxyServer({target: this.target_url, changeOrigin: true, secure: true, xfwd: false})

    // Apply any listeners to the Proxy
    ProxyServer.applyListeners(this.proxy)
  }

  process(req, res) {

    if (this.root_rewrite) {
      var parsed_url = url.parse(req.url);
      if(parsed_url.pathname == '/') {
        res.writeHead(302,  { Location: `${this.root_rewrite}`} )
        return res.end();
      }
    }

    // Remove the 'Authorization' header here rather in the listener
    delete req.headers['Authorization'];
    delete req.headers['authorization'];

    this.proxy.web(req, res)
  }

  close(callback) {
    this.proxy.close()
    callback()
  }

  static applyListeners(proxyServer) {

    // Error Listener
    proxyServer.on('error', (err, req, res) => {
      logger.error(`Proxy Error: ${err.message}`)
      res.writeHead(500, {
        'Content-Type': 'text/plain'
      });
      res.end('There was an error resolving your request. If the problem persists, please contact the administrator')
    });

    // Proxy Request Listener
    proxyServer.on('proxyReq', (proxyReq, req, res)  => {
      // This doesnt seem to work for dealing with removal of headers. Seems
      // to cause a bit of a race condition
    })
  }

}

module.exports = ProxyServer
